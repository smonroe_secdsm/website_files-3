# How to contribute:

## Updating data.json in the browser
![Alt text](https://secdsm.org/images/update_data-json.gif)


## General Flow
1. create a fork
1. Make changes to data.json  
1. create pull request of the local fork to pilot  
1. once approved - Jenkins will deploy to [https://pilot.secdsm.org/](https://pilot.secdsm.org)
1. validate changes to pilot
1. repeat until pilot looks the way it should
1. create PR from pilot to master.


##  Merge Access to Pilot
The idea is that you should be able to approve your own merge requests to pilot.secdsm.org  
Hit up zoomequipd in slack for merge access to pilot

## The deployment process
When a PR is approved to either pilot or master the following process kicks off  

1. Jenkins gets a job to build website_files
1. Jenkins checks that the data.json files is able to be parsed by running "check-json.rb"
    * if the check exits non-zero the build fails
1. Jenkins runs the render.rb script
    * if the script exits non-zero the build fails
1. The previous version of the site is deleted  - /var/www/html/[pilot/prod]
1. The current version of the site is copied over to /var/www/html/[pilot/prod]
1. If there is a index_compiled.html file, it is moved to /var/www/html/[pilot/prod]/index.html
    * if there is not a index_compiled.html the build fails
1. Files are removed from /var/www/html/[pilot/prod]/
    * .git
    * data.json
    * template.html
    * render.rb
    * check-json.rb
    * .gitignore
1. The build status is then reported back to bitbucket.


---------------------------------------
# index.html details
This file contains a default "maintenance" style page. In the event that a deployment process, this serves as a catchall to ensure a page is still available.   
Generally speaking, anytime ["template.html"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-template.html) is updated, this page will also need to be updated  

# template.html details
This file contains the shell that is used by the ["render.rb"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-render.rb) script to build out ["index_compiled.html"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-index_compiled.html)

# render.rb 
Ruby script that depends on the ["Liquid"](https://shopify.github.io/liquid/) and Json gems.   
We use Liquid to build out index_compiled.html based on the content of the ["data.json"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-data.json)  

# index_compiled.html
This file is created by the ["render.rb"](https://bitbucket.org/secdsm/website_files/overview#markdown-header-render.rb) script.   
It file gets moved after being compiled to /var/www/html/[prod|pilot]/index.html (overwriting the default "maintenance" style page)  
this file should never be committed to git and is included in the .gitignore file

# data.json details
data.json controls all the elements that change month to month. Adding a talk/event/etc all happens here.  
This is the main file that should be getting modified.  


Must be a json formatted file - this controls all the dynamic fields for the [secdsm.org](https://secdsm.org) page  
There are two main sections to the data.json file

  1. [schedules](https://bitbucket.org/secdsm/website_files/overview#markdown-header-schedules)
  1. [cons](https://bitbucket.org/secdsm/website_files/overview#markdown-header-cons)
  1. [monthly](https://bitbucket.org/secdsm/website_files/overview#markdown-header-monthly)

## Schedules
an array of hashes containing the details for the schedule section of the website. 

* Each element in the array should contain a full month's worth of details.
* this includes an sponsors, introtalks, tooltalks, features, and subgroups elements - see below for details on those keys.

### Keys Available to the schedule

* "date" - DD MMM YYYY
* "sponsors" - an array of hashes containing information about sponsors  
There is a hard limit of 2 sponsors per meeting
    * Keys Available to elements in sponsor hash
        * "sponsor" - Name of the sponsor
        * "image" - Name of the image in the images/sponsorlogos/ directory 
        * "url" - a link to the sponsor's page
* "introtalks" - a hash containing the intro talk details
    * Keys Available to elements in introtalks array
        * "title"
        * "alert" - optional
            * "type"  - one of  "info" "success" "warning" "danger"
            * "message"        * "time"
        * "description"
        * "speaker.name" - optional (not an object here)
* "tooltalks" - an array containing tools talks
    * Keys Available to elements in tooltalks array
        * "title"
        * "alert" - optional
            * "type"  - one of  "info" "success" "warning" "danger"
            * "message"        * "description" - required
        * "time"
        * "speaker"  - an array containing speaker details
            * "name"
            * "bio" - optional
* "features" - an array containing feature talks
    * Keys Available to elements in features arrays
        * "title"
        * "alert" - optional
            * "type"  - one of  "info" "success" "warning" "danger"
            * "message"        * "description" - optional, but generally desired
        * "time"
        * "speaker"  - an array containing speaker details
            * "name"
            * "bio" - optional
* "subgroups" - an array containing subgroup details
    * Keys Available to elements in subgroups arrays
        * "title"
        * "alert" - optional
            * "type"  - one of  "info" "success" "warning" "danger"
            * "message"
        * "description" - optional, but generally desired
        * "time" - optional
        * "speaker"  - an array containing speaker details
            * "name" - optional
            * "bio" - optional


## Cons
An array of hashes, each hash should contain details for one conference
### Keys available to elements in "cons" array
* "name"
* "description"
* "link"
* "date"
* "location"
* "cost"
* "details"  - an optional hash containing details of the conference 
    * Keys available to elements in the details hash
        * "schedule" - an optional array, consisting of one entry for each day of the conference
            * keys available to elements inside of the schedule array
                * "day"
                * "date"
                * "start_time"
                * "end_time"
        * "travel" - a optional hash, consisting of a depart and return hash
            * keys available to elements inside the travel hash
                * "depart"
                    * keys available to elements inside the depart/return hash
                        * "day"
                        * "date"
                        * "time"
                * "return"
                    * keys available to elements inside the depart/return hash
                        * "day"
                        * "date"
                        * "time"

## Monthly
An array of hashes, the outermost array is each month
* Each element in the "monthly" array should contain a full month's worth of events.  
* Each element in the "events" array becomes a collapsible box 
* Order matters when adding months to the events section.  
  * The first event element will be the element that is expanded.
  * Months are ascending

### Keys available to elements in "monthly" array
* "name" - the name of the month
* "events" - an array of hashes, each hash is one event
    * "url"
    * "name"
    * "time"  
    * "description" - optional
    * "geolocation" - optional
        * You can embed a google maps by:  
            1. Go to http://maps.google.com  
            1. Select a point of interest  
            1. Click share  
            1. Click the `Embed map` tab    
            1. Copy the link in the `src` attribute 
  